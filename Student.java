public class Student {
	private String npm;
	private String nama;
	private double gpa;

	public Student(String npm, String nama, double gpa) {
		this.npm = npm;
		this.nama = nama;
		this.gpa = gpa;
	}

	public String getNpm() {
		return this.npm;
	}

	public void setNpm(String npm) {
		this.npm = npm;
	}

	public String getNama() {
		return this.nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public double getGpa() {
		return this.gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

}
