public class Dosen {
	private String nip;
	private String namaDosen;

	public Dosen(String nip, String namaDosen) {
		this.nip = nip;
		this.namaDosen = namaDosen;
	}

	public String getNip() {
		return this.nip;
	}

	public void setNip(String nip) {
		this.nip = nip;
	}

	public String getNamaDosen() {
		return this.namaDosen;
	}

	public void setNamaDosen(String namaDosen) {
		this.namaDosen = namaDosen;
	}
}
